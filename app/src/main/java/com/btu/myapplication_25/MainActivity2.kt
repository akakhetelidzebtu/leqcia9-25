package com.btu.myapplication_25

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity2 : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)
        Log.d("activitylifecycle", "onCreateActivity2")
    }


    override fun onStart() {
        super.onStart()
        Log.d("activitylifecycle", "onStartActivity2")
    }

    override fun onResume() {
        super.onResume()
        Log.d("activitylifecycle", "onResumeActivity2")
    }

    override fun onPause() {
        super.onPause()
        Log.d("activitylifecycle", "onPauseActivity2")
    }
    override fun onStop() {
        super.onStop()
        Log.d("activitylifecycle", "onStopActivity2")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d("activitylifecycle", "onDestroyActivity2")
    }
}